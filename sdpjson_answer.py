import udp_echo_server as udpServer
import sdp_transform
import json


class EchoServerProtocol:
    def connection_made(self, transport):
        self.transport = transport

    def datagram_received(self, dataMessage, address):
        jsonMessageReceived = json.loads(dataMessage.decode())
        print('Received %r from %s' % (jsonMessageReceived, address))
        sendingMessage = sdp_transform.parse(jsonMessageReceived['sdp'])

        sendingMessage['media'][0]['port'] = 34543
        sendingMessage['media'][1]['port'] = 34543 # Change port

        modified_sendingMessage = sdp_transform.write(sendingMessage)

        answerMessage = {
            "type": "answer",
            "sdp": modified_sendingMessage}

        print('Send %r to %s' % (answerMessage, address))
        jsonAnswerMessage = json.dumps(answerMessage)
        self.transport.sendto(jsonAnswerMessage.encode(), address) # Send the answer back to the client.


async def main():
    print('Running UDP server')

    loop = udpServer.asyncio.get_running_loop()
    transport, protocol = await loop.create_datagram_endpoint(
        lambda: EchoServerProtocol(), local_addr=('127.0.0.1', 9999))

    try:
        await udpServer.asyncio.sleep(3600)  # Serve for 1 hour.
    finally:
        transport.close()

if __name__ == '__main__':
    udpServer.asyncio.run(main())