import udp_echo_server as udpServer
import sdp_transform


class EchoServerProtocol():
    def connection_made(self, transport):
        self.transport = transport

    def datagram_received(self, dataMessage, address):
        messageReceived = dataMessage.decode()
        print('Received %r from %s' % (messageReceived, address))
        sendingMessage = sdp_transform.parse(messageReceived)
        print(sendingMessage)
        sendingMessage['media'][0]['port'] = 34543
        sendingMessage['media'][1]['port'] = 34543 # Change port
        print(sendingMessage['media'][0]['port'], sendingMessage['media'][1]['port'])
        modified_sendingMessage = sdp_transform.write(sendingMessage)
        print('Send %r to %s' % (modified_sendingMessage, address))
        self.transport.sendto(modified_sendingMessage.encode(), address)


async def main():
    print('Running UDP server')

    loop = udpServer.asyncio.get_running_loop()
    transport, protocol = await loop.create_datagram_endpoint(
        lambda: EchoServerProtocol(), local_addr=('127.0.0.1', 9999))


    try:
        await udpServer.asyncio.sleep(3600)  # Serve for 1 hour.
    finally:
        transport.close()

if __name__ == '__main__':
    udpServer.asyncio.run(main())


