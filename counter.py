import asyncio


async def counter(id, time):
    print(f"Counter {id}: starting...")
    await asyncio.sleep(1)  # -> Salgo del proceso durante un segundo,
    # Vuelve a activarse pasado el segundo y cuando no haya tareas en ejecucion
    for i in range(time + 1):
        await asyncio.sleep(1)
        print(f"Counter {id}: {i}")
    print(f"Counter {id}: finishing...")
    await asyncio.sleep(0.5)


async def main():
    await asyncio.gather(counter('A', 4), counter('B', 2), counter('C', 6))

asyncio.run(main())
