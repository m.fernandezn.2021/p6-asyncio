import udp_echo_client as udpClient
import sdp_transform
import json


async def main():
    # Get a reference to the event loop as we plan to use
    # low-level APIs.
    loop = udpClient.asyncio.get_running_loop()

    on_con_lost = loop.create_future()  # Creates a future that will be set when the connection is closed.

    offerMessage_sdp = {
        'version': 0,
        'origin': {
            'username': 'user',
            'sessionId': 434344,
            'sessionVersion': 0,
            'netType': 'IN',
            'ipVer': 4,
            'address': '127.0.0.1'},
        'name': 'Session',
        'timing': {
            'start': 0,
            'stop': 0},
        'connection': {
            'version': 4,
            'ip': '127.0.0.1'},
        'media': [{'rtp': [{'payload': 0, 'codec': 'PCMU', 'rate': 8000},
                           {'payload': 96, 'codec': 'opus', 'rate': 48000}],
                   'type': 'audio',
                   'port': 54400,
                   'protocol': 'RTP/SAVPF',
                   'payloads': '0 96',
                   'ptime': 20,
                   'direction': 'sendrecv'},
                  {'rtp': [{'codec': 'H264', 'payload': 97, 'rate': 90000},
                           {'codec': 'VP8', 'payload': 98, 'rate': 90000}],
                   'type': 'video',
                   'port': 55400,
                   'protocol': 'RTP/SAVPF',
                   'payloads': '97 98',
                   'direction': 'sendrecv'}]}

    offerMessage_sdpModified = sdp_transform.write(offerMessage_sdp)
    offerMessage2 = {
        "type": "offer",
        "sdp": offerMessage_sdpModified}

    jsonMessage = json.dumps(offerMessage2)
    transport, protocol = await loop.create_datagram_endpoint(
        lambda: udpClient.EchoClientProtocol(jsonMessage, on_con_lost),
        remote_addr=('127.0.0.1', 9999))  # Creates a datagram endpoint

    try:
        await on_con_lost
    finally:
        transport.close()

if __name__ == '__main__':
    udpClient.asyncio.run(main())
