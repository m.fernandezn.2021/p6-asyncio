import asyncio


async def hello():
    print('¡Hola ...')
    await asyncio.sleep(1)
    print('... mundo!')


async def main():
    await asyncio.gather(hello(), hello(), hello())

if __name__ == '__main__':
    asyncio.run(main())
