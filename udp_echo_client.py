import asyncio


class EchoClientProtocol:
    def __init__(self, message, on_con_lost):
        self.message = message
        self.on_con_lost = on_con_lost
        self.transport = None

    def connection_made(self, transport):
        self.transport = transport
        print('Send:', self.message)
        self.transport.sendto(self.message.encode())

    def datagram_received(self, data, addr):
        print("Received:", data.decode())

        print("Close the socket")
        self.transport.close()

    def error_received(self, exc):
        print('Error received:', exc)

    def connection_lost(self, exc):
        print("Connection closed")
        self.on_con_lost.set_result(True)


async def main():
    # Get a reference to the event loop as we plan to use
    # low-level APIs.
    loop = asyncio.get_running_loop()

    on_con_lost = loop.create_future() # Creates a future that will be set when the connection is closed.
    message = "Hello World!"

    transport, protocol = await loop.create_datagram_endpoint(
        lambda: EchoClientProtocol(message, on_con_lost), remote_addr=('127.0.0.1', 9999)) # Create a datagram endpoint
    # and send the message, then waits for the connection  being closed and closes it.

    try:
        await on_con_lost # Wait for the connection to be closed.
    finally:
        transport.close() # Close the transport.

if __name__ == '__main__':
    asyncio.run(main()) # Run the event loop.
